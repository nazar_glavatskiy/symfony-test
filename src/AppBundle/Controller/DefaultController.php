<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Books;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function listAction()
    {
        // Get all books
        $books = $this->getDoctrine()
            ->getRepository('AppBundle:Books')
            ->getBooks();

        // Get all authors
        $authors = $this->getDoctrine()
            ->getRepository('AppBundle:Authors')
            ->findAll();

        // Get all genres
        $genres = $this->getDoctrine()
            ->getRepository('AppBundle:Genres')
            ->findAll();

        return $this->render('default/index.html.twig', array(
            'books'     => $books,
            'authors'   => $authors,
            'genres'    => $genres,
        ));
    }


    /**
     * @Route("/create", name="create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        if (!$data = $this->validateForm($request)) {
            return new JsonResponse([
                'status' => false
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $book = new Books();
        $book->setTitle($data['title'])
            ->setAuthorId($data['authorId'])
            ->setAuthors($data['author'])
            ->setGenreId($data['genreId'])
            ->setGenres($data['genre'])
            ->setLanguage($data['language'])
            ->setPublicationDate($data['publicationDate'])
            ->setIsbnNumber($data['isbnNumber']);

        $em->persist($book);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }

    /**
     * @Route("/edit", name="edit")
     * @Method("POST")
     */
    public function editAction(Request $request)
    {
        if (!$data = $this->validateForm($request)) {
            return new JsonResponse([
                'status' => false
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $data['book']->setTitle($data['title'])
            ->setAuthorId($data['authorId'])
            ->setAuthors($data['author'])
            ->setGenreId($data['genreId'])
            ->setGenres($data['genre'])
            ->setLanguage($data['language'])
            ->setPublicationDate($data['publicationDate'])
            ->setIsbnNumber($data['isbnNumber']);

        $em->persist($data['book']);
        $em->flush();

        return new JsonResponse([
            'status' => true
        ]);
    }

    /**
     * Validate form
     *
     * @param Request $request
     * @return bool|array
     */
    private function validateForm(Request $request)
    {
        $data['bookId'] = (int)$request->request->get('book');

        if ($data['bookId']) {
            // Get book by ID
            $data['book'] = $this->getDoctrine()
                ->getRepository('AppBundle:Books')
                ->find($data['bookId']);

            // Check book exist
            if (!$data['book']) {
                return false;
            }
        }

        $data['title'] = strip_tags($request->request->get('title'));
        $data['authorId'] = (int)$request->request->get('author');
        $data['genreId'] = (int)$request->request->get('genre');
        $data['language'] = $request->request->get('language');
        $data['publicationDate'] = (int)$request->request->get('date');
        $data['isbnNumber'] = $request->request->get('isbn');

        // Check ISBN-13
        if(!preg_match('/^[0-9]{3}-[0-9]{10}$/', $data['isbnNumber'])) {
            return false;
        }

        // Get author by ID
        $data['author'] = $this->getDoctrine()
            ->getRepository('AppBundle:Authors')
            ->find($data['authorId']);

        // Check author exist
        if(!$data['author']) {
            return false;
        }

        // Get genre by ID
        $data['genre'] = $this->getDoctrine()
            ->getRepository('AppBundle:Genres')
            ->find($data['genreId']);

        // Check genre exist
        if(!$data['genre']) {
            return false;
        }

        return $data;
    }
}
