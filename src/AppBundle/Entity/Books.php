<?php

namespace AppBundle\Entity;

/**
 * Books
 */
class Books
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $authorId;

    /**
     * @var int
     */
    private $genreId;

    /**
     * @var string
     */
    private $language;

    /**
     * @var \DateTime
     */
    private $publicationDate;

    /**
     * @var string
     */
    private $isbnNumber;

    /**
     * @var string
     */
    private $image;

    private $authors;

    private $genres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     *
     * @return Book
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set genreId
     *
     * @param integer $genreId
     *
     * @return Book
     */
    public function setGenreId($genreId)
    {
        $this->genreId = $genreId;

        return $this;
    }

    /**
     * Get genreId
     *
     * @return int
     */
    public function getGenreId()
    {
        return $this->genreId;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Book
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set publicationDate
     *
     * @param \DateTime $publicationDate
     *
     * @return Book
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * Get publicationDate
     *
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * Set isbnNumber
     *
     * @param string $isbnNumber
     *
     * @return Book
     */
    public function setIsbnNumber($isbnNumber)
    {
        $this->isbnNumber = $isbnNumber;

        return $this;
    }

    /**
     * Get isbnNumber
     *
     * @return string
     */
    public function getIsbnNumber()
    {
        return $this->isbnNumber;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Book
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set authors
     *
     * @param \AppBundle\Entity\Authors $authors
     *
     * @return Books
     */
    public function setAuthors(\AppBundle\Entity\Authors $authors = null)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return \AppBundle\Entity\Authors
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set genres
     *
     * @param \AppBundle\Entity\Genres $genres
     *
     * @return Books
     */
    public function setGenres(\AppBundle\Entity\Genres $genres = null)
    {
        $this->genres = $genres;

        return $this;
    }

    /**
     * Get genres
     *
     * @return \AppBundle\Entity\Genres
     */
    public function getGenres()
    {
        return $this->genres;
    }
}
