<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180413121614 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // Genres
        $genres = [
            'Programming',
            'Children`s Book',
        ];

        // Authors
        $authors = [
            [
                'Luke',
                'Welling',
            ],
            [
                'Laura',
                'Thomson',
            ],
            [
                'Antoine',
                'de Saint-Exupery',
            ],
            [
                'David',
                'Sawyer McFarland',
            ],
        ];

        // SQL for Genres
        $sqlGenres = 'INSERT INTO `genres` (name) VALUES ';

        $genres = array_map(function($genre) {
            return "('{$genre}')";
        }, $genres);

        $sqlGenres .= implode(',', $genres);

        // SQL for Authors
        $sqlAuthors = 'INSERT INTO `authors` (firstname, lastname) VALUES ';

        $authors = array_map(function($author) {
            return "('{$author[0]}', '{$author[1]}')";
        }, $authors);

        $sqlAuthors .= implode(',', $authors);

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql($sqlGenres);
        $this->addSql($sqlAuthors);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM authors');
        $this->addSql('DELETE FROM genres');

    }
}
